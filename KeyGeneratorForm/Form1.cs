﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KeyGeneratorForm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }


        private void button_GenerateKey_Click(object sender, EventArgs e)
        {
            textBox2.Text = KeyGenerator.Helper.GenerateKey(textBox1.Text);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            

        }

        private void buttonReverseKey_Click(object sender, EventArgs e)
        {
            textBox2.Text = KeyGenerator.Helper.ReverseKey(textBox1.Text);
        }
    }
}
