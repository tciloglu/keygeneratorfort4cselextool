﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeyGenerator
{
    public class Helper
    {

        static String alphabet = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        public static string GenerateKey(string key)
        {
            string cleanedKey = key.Replace(" ", "").Trim();
            string newKey = decrypt(key);
            return newKey;
        }

        public static string ReverseKey(string key)
        {
            string cleanedKey = key.Replace(" ", "").Trim();
            string newKey = encrypt(key);
            return newKey;
        }


        private static String encrypt(String originalString)
        {
            String returnString = "";
            int shift = alphabet.Length / 2;

            foreach (char c in originalString)
            {
                if(c=='-')
                {
                    returnString += '-';
                    continue;
                }

                int nextIndex = alphabet.IndexOf(c) + shift;

                if (nextIndex > alphabet.Length)
                    nextIndex = nextIndex - alphabet.Length;

                returnString += alphabet[nextIndex];
                shift = alphabet.IndexOf(alphabet[nextIndex]);
            }

            return returnString;
        }

        private static String decrypt(String encryptedString)
        {
            String returnString = "";
            int shift = alphabet.Length / 2;

            foreach (char c in encryptedString)
            {
                if (c == '-')
                {
                    returnString += '-';
                    continue;
                }

                int nextIndex = alphabet.IndexOf(c) - shift;

                if (nextIndex < 0)
                    nextIndex = alphabet.Length + nextIndex; // nextIndex is negative so we are decreasing regardless

                returnString += alphabet[nextIndex];
                shift = alphabet.IndexOf(c);
            }

            return returnString;
        }
    }
}
